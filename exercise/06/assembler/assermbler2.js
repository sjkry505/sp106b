var fs = require("fs");
var file = process.argv[2];
var targetfile = process.argv[3];
var ex, code, address, newsym = 16;
var writefile = fs.createWriteStream(targetfile+".hack");

var dtable = {
  ""   :0b000,
  "M"  :0b001,
  "D"  :0b010,
  "MD" :0b011,
  "A"  :0b100,
  "AM" :0b101,
  "AD" :0b110,
  "AMD":0b111
}

var jtable = {
  ""   :0b000,
  "JGT":0b001,
  "JEQ":0b010,
  "JGE":0b011,
  "JLT":0b100,
  "JNE":0b101,
  "JLE":0b110,
  "JMP":0b111
}

var ctable = {
  "0"   :0b0101010,
  "1"   :0b0111111,
  "-1"  :0b0111010,
  "D"   :0b0001100,
  "A"   :0b0110000, 
  "M"   :0b1110000,
  "!D"  :0b0001101,
  "!A"  :0b0110001, 
  "!M"  :0b1110001,
  "-D"  :0b0001111,
  "-A"  :0b0110011,
  "-M"  :0b1110011,
  "D+1" :0b0011111,
  "A+1" :0b0110111,
  "M+1" :0b1110111,
  "D-1" :0b0001110,
  "A-1" :0b0110010,
  "M-1" :0b1110010,
  "D+A" :0b0000010,
  "D+M" :0b1000010,
  "D-A" :0b0010011,
  "D-M" :0b1010011,
  "A-D" :0b0000111,
  "M-D" :0b1000111,
  "D&A" :0b0000000,
  "D&M" :0b1000000,
  "D|A" :0b0010101,
  "D|M" :0b1010101
}

var stable = {
  "R0"  :0,
  "R1"  :1,
  "R2"  :2,
  "R3"  :3,
  "R4"  :4,
  "R5"  :5,
  "R6"  :6,
  "R7"  :7,
  "R8"  :8,
  "R9"  :9,
  "R10" :10,
  "R11" :11,
  "R12" :12,
  "R13" :13,
  "R14" :14,
  "R15" :15,
  "SP"  :0,
  "LCL" :1,
  "ARG" :2,
  "THIS":3, 
  "THAT":4,
  "KBD" :24576,
  "SCREEN":16384
};

parser(file);

function parser (testfile) {
    var text = fs.readFileSync(testfile, "utf8");
    var lines = text.split(/\r?\n/);
    for(var i=0;i<lines.length;i++) {
        deal(lines[i]);
    }
}

function deal (instruction) {
    instruction = instruction.trim();
    if (instruction[0] == "/" || instruction == "")
        return;
    for(var i=0; i<instruction.length; i++) {
        if (instruction[i] === " ") {
            ex = i;
            break;
        }
    }
    compare(instruction);
}

function compare (item) {
    code = item.substring(0,1);
    if (code == "@") {
        transfer(item, "a");
    } else if (code == "(") {
    } else {
        transfer(item, "c");
    }
}

function transfer(ins, type) {
    var ccode,dcode,jcode,cb,db,jb;
    if(type === "a") {
        ins = ins.substring(1);
        if (ins.match(/^\d+$/)) {
            address = parseInt(ins);
        } else {
            address = stable[ins];
            if (typeof address === "undefined") {
                address = newsym;
                stable[ins] = newsym;
                newsym++;
            }
        }
        address = toBinary(address, 16);
    } else if (type === "c") {
        ins = ins.substring(0, ex);
        for(var i=0;i<ins.length;i++) {
            if (ins[i] === "=") {
                dcode = dtable[ins.substring(0,i)];
                ccode = ctable[ins.substring(i+1)];
                db = toBinary(dcode,3);
                cb = toBinary(ccode,7);
                address = "111"+cb+db+"000";
            } else if (ins[i] === ";") {
                ccode = ctable[ins.substring(0,i)];
                jcode = jtable[ins.substring(i+1)];
                jb = toBinary(jcode,3);
                cb = toBinary(ccode,7);
                address = "111"+cb+"000"+jb;
            }
        }
    }
    writefile.write(address+"\n");
    console.log("%s : %s", ins, address);
}

function toBinary(addr, lens) {
    addr = addr.toString(2);
    while(addr.length < lens) {
        addr = "0"+addr;
    }
    return addr;
}
writefile.end();