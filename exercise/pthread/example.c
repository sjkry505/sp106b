#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void *hello (void *argu) {
    while(1) {
        printf("hello !\n");
        sleep(1);
    }
    return NULL;
}

void *Jerry (void *argu) {
    while(1) {
        printf("HI, Jerry\n");
        sleep(2);
    }
    return NULL;
}

void main () {
    pthread_t thread1, thread2;
    pthread_create(&thread1, NULL, &hello, NULL);
    pthread_create(&thread2, NULL, &Jerry, NULL);

    while(1) {
        printf("-------------------\n");
        sleep(1);
    }
    return;
}