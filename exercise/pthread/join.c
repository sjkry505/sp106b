#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void *count (void *argu) {
    int i;
    for(i=0; i<=10; i++) {
        printf("the thread number is %d\n", i);
        sleep(2);
    }
    return NULL;
}

void main () {
    int i;
    pthread_t thread1;
    pthread_create(&thread1, NULL, &count, NULL);
    pthread_join(thread1, NULL);

    for(i=0; i<=10; i++) {
        printf("the main number is %d\n", i);
        sleep(1);
    }
    return;
}