#include "rlib.c"

void Exp();
void T();
void F();
void Id();

// === BNF Grammar =====
// Assign = id '=' Exp
// Exp = T ([+-] T)?        
// T = F ([*/] F)?
// F = [0-9] | (Exp)

int main(int argc, char * argv[]) {
    int i = 0;
    while (i<10) {
        Id();
        Exp();
        printf(";\n");
        i++;
    }

}

char* Idlist[] = {"a", "b", "c", "x", "y", "z"};

void Id() {
    printf("%s = ", randSelect(Idlist, 6));
}

void Exp() {
    T();
	if (randInt(10) < 3) {
       printf("%c", randChar("+-"));
	   T();
	}
}

void T() {
    F();
	if (randInt(10) < 7) {
		printf("%c", randChar("*/"));
		F();
	}
}

void F() {
    if (randInt(10) < 8) {
		printf("%c", randChar("0123456789"));
	} else {
		printf("(");
		Exp();
		printf(")");
	}
}
