var U = require('./util')
var next = U.print

// === BNF Grammar =====
// Assign = id '=' Exp
// Exp = T ([+-] T)?        
//'?' 代表有執行或不執行(只執行一次) , '*' 代表執行零次以上(彈性最佳) , '+' 代表執行一次以上
// T = F ([*/] F)?
// Exp = T ([+-] T)? , T = F ([*/] F)? => 有加減乘除的優先順序
// F = [0-9] | (Exp)

var idList = ['x', 'y', 'z', 'a', 'b', 'c']

function Assign () {
  id()
  next('=')
  Exp()
  next('\n')
}

function id () {
  next(U.randSelect(idList))      // 從idList陣列中，隨機選取一個id
}

function Exp () {
  T()
// 若 U.randInt(1, 10) > 5 , 執行 EXP = T , 否則執行 Exp = T ([+-] T)
  if (U.randInt(1, 10) <= 5) {     
    next(U.randChar('+-'))
    T()
  }
}

function T () {
  F()
// 若 U.randInt(1, 10) > 5 , 執行 T = F , 否則執行 T = F ([*/] F)
  if (U.randInt(1, 10) <= 5) {
    next(U.randChar('*/'))
    F()
  }
}

function F () {
// 若 U.randInt(1, 10) >= 8 , 執行 F = (Exp) , 否則執行 F = [0-9]
  if (U.randInt(1, 10) < 7) {
    next(U.randChar('0123456789'))
  } else {
    next('(')
    Exp()
    next(')')
  }
}

for (var i = 0; i < 10; i++) {
  Assign()
}
