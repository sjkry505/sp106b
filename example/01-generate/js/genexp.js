var U = require('./util')
var next = U.print

// === BNF Grammar =====
// E = T [+-*/] E | T
// T = [0-9] | (E)

function E () {
  if (U.randInt(1, 10) <= 5) {     //取到的數字<=5，執行 T [+-*/] E
    T()
    next(U.randChar('+-*/'))       //隨機選取 +-*/ 其中一個
    E()                            //遞迴
  } else {                         // 執行 T
    T()
  }
}

function T () {
  if (U.randInt(1, 10) < 7) {          // 執行[0-9]=> 取到的數字<7，從0-9中隨機選取一個數字
    next(U.randChar('0123456789'))
  } else {                             // 執行 (E)
    next('(')
    E()
    next(')')
  }
}

for (var i = 0; i < 10; i++) {        // 印出10個句子
  E()
  next('\n')
}