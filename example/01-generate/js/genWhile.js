var U = require('./util')
var next = U.print
var p = U.probability

// === BNF Grammar =====
// StmtList = Stmt+
// Stmt = While | Assign
// While = while '(' Exp ')' { StmtList }
// Assign = id '=' Exp
// Exp = T ([+-] T)?
// T = F ([*/] F)?
// F = [0-9a-cx-z] | (Exp)

var level = 0

function indent() {
// str.padStart(num) => 在字串前加入str，若字元數小於num，則印出原本的str
  next(''.padStart(level * 2))
}

function StmtList () {
// while中至少會有一行運算式，所以使用do while 迴圈，當p(0.5)成立，則會繼續執行
  do {
    Stmt()
  } while (p(0.5))
}

function Stmt () {
// p(0.5)，執行while()；否則執行indent(); Assign() (要有indent()是為了程式的排版)
  if (p(0.5)) {
    While()
  } else {
    indent(); Assign()
  }
}

function While () {
// 若產生多層while，則要考慮排版問題，因此在印出while前，必須有indent()
  indent(); next('while ('); Exp(); next(') {\n')
// level ++ 會增加縮排，在同一個階段的程式結束後，用level--來減少縮排
  level ++; StmtList(); level --
// indent() 決定在 } 出現前的縮排
  indent(); next('}\n')
}

function Assign () {
  id(); next('='); Exp(); next('\n')
}

var idList = ['x', 'y', 'z', 'a', 'b', 'c']

function id () {
  next(U.randSelect(idList))
}

function Exp () {
  T()
  if (p(0.5)) {
    next(U.randChar('+-'))
    T()
  }
}

function T () {
  F()
  if (p(0.5)) {
    next(U.randChar('*/'))
    F()
  }
}

function F () {
  if (p(0.7)) {
    next(U.randChar('0123456789abcxyz'))
  } else {
    next('('); Exp(); next(')')
  }
}

While()